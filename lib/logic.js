'use strict';

function hashPDF(s){
    var hash = 0;
	if (s.length == 0) return hash;
	for (var i = 0; i < s.length; i++) {
        char = s.charCodeAt(i);
		hash = ((hash<<5)-hash)+char;
		hash = hash & hash; // Convert to 32bit integer
	}
	return String(hash);
}

function randomString (strLength) {
    var result = [];
    strLength = strLength || 5;
    charSet = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
    while (--strLength) {
        result.push(charSet.charAt(Math.floor(Math.random() * charSet.length)));
    }
    return result.join('');
}

 // Current participant must be a DP person or admin.
 function onDpTransaction(DpTransaction) {
    let currentParticipant = getCurrentParticipant();
  	let fullyQualifiedType = currentParticipant.getFullyQualifiedType();
    if (fullyQualifiedType == 'org.hyperledger.composer.system.NetworkAdmin') {
        return 
    } else if (fullyQualifiedType.includes('org.coma.biznet.Delivery')) {
        return
    } else {
        throw new Error('Transaction can only be submitted by a delivery partner')
    }
}

 // Current participant must be a Customer person or admin.
 function onCusTransaction(DpTransaction) {
    let currentParticipant = getCurrentParticipant();
  	let fullyQualifiedType = currentParticipant.getFullyQualifiedType();
    if (fullyQualifiedType == 'org.hyperledger.composer.system.NetworkAdmin') {
        return 
    } else if (fullyQualifiedType.includes('org.coma.biznet.Customer')) {
        return
    } else {
        throw new Error('Transaction can only be submitted by a customer')
    }
}

 // Current participant must be a NetworkAdmin.
 function onAdmiTransaction(DpTransaction) {
    let currentParticipant = getCurrentParticipant();
    let fullyQualifiedType = currentParticipant.getFullyQualifiedType();
    if (fullyQualifiedType == 'org.hyperledger.composer.system.NetworkAdmin') {
        return 
    }  else {
        throw new Error('Transaction can only be submitted by a customer')
    }
}

/**
 * Create Request
 * @param {org.coma.biznet.CreateRequest} CreateRequest
 * @transaction
 */
function onCreateRequest(CreateRequest) {
    onCusTransaction();
    // Get the order asset registry.
    return getAssetRegistry('org.coma.biznet.Order')
    .then(function (orderAssetRegistry) {
        // Get the factory for creating new asset instances.
        var factory = getFactory();
        // Create the order.
        var request = factory.newResource('org.coma.biznet', 'Order', CreateRequest.orderId);
        let currentParticipant = getCurrentParticipant();

        console.log("Current Participant");
        console.log(currentParticipant);

        request.orderName   = CreateRequest.orderName;
        request.scope       = CreateRequest.scope;
        request.skillKnowledge = CreateRequest.skillKnowledge;
        request.orderType = CreateRequest.orderType;
        request.startDate = CreateRequest.startDate;
        request.endDate = CreateRequest.endDate;
        request.cusComments = CreateRequest.cusComments;
        request.deliveryPartner = CreateRequest.deliveryPartner;
        request.contractPartner = CreateRequest.contractPartner;
        request.creationDate = new Date();
        request.lastUpdateDate = new Date();

        if (CreateRequest.rfiRelease === true) {
            request.orderStatus = 'RFI_DP'
        } else {
            request.orderStatus = 'DRAFT'
        }
        request.customer = currentParticipant.company
        return orderAssetRegistry.add(request);
    })
}

/**
 * Submit Estimate
 * @param {org.coma.biznet.SubmitEstimate} SubmitEstimate
 * @transaction
 */
function onSubmitEstimate(SubmitEstimate) {
    onDpTransaction();
  	var assetRegistry;
    var id = SubmitEstimate.relatedOrder.orderId;
    var factory = getFactory();
    return getAssetRegistry('org.coma.biznet.Order')
        .then(function(ar) {
            assetRegistry = ar;
            return assetRegistry.get(id);
        })
        .then(function(order) {
            if (order.orderStatus !== 'RFI_DP') {
                throw new Error('Status is not RFI_DP');
            }
            if (SubmitEstimate.statusUpd === true) {
                order.orderStatus = 'RFI_CLARIFICATION'
            }
            order.initialEstimate = SubmitEstimate.newEstimate;
            order.lastUpdateDate = new Date();
            return assetRegistry.update(order);
        })
        .then(function(){
            if (SubmitEstimate.statusUpd === true) {
                var SubmittedEvent = factory.newEvent('org.coma.biznet', 'EstimateSubmittedEvent');
                SubmittedEvent.orderId = id;
                emit(SubmittedEvent);
            }
    	});
}

/**
 * Close Order
 * @param {org.coma.biznet.CloseOrder} CloseOrder
 * @transaction
 */
function onCloseOrder(CloseOrder) {
    onCusTransaction();
    var assetRegistry;
    var factory = getFactory();
    var id = CloseOrder.relatedOrder.orderId;
    var relatedOrder;
    return getAssetRegistry('org.coma.biznet.Order')
    .then(function(ar) {
        assetRegistry = ar;
        return assetRegistry.get(id);
    })
    .then(function(order){
        relatedOrder = order;
        order.cusComments = CloseOrder.comment;
        order.orderStatus = CloseOrder.status;
        order.lastUpdateDate = new Date();
        return assetRegistry.update(order);
    })
    .then(function(){
        var eventOrderClosed = factory.newEvent('org.coma.biznet', 'OrderClosed');
        eventOrderClosed.relatedOrder = relatedOrder;
        eventOrderClosed.identity = getCurrentIdentity();
        return emit(eventOrderClosed);
    });
}

function _addCustomer(oData) {
    oData.ENTITYNAME = 'Customer';
    oData.role = 'CUSTOMER';
    return __addBusinessPartner(oData);
}
function _addDeliveryPartner(oData) {
    oData.ENTITYNAME = 'DeliveryPartner';
    oData.role = 'DELIVERY_PARTNER';
    return __addBusinessPartner(oData);
}
function _addContractPartner(oData) {
    oData.ENTITYNAME = 'ContractPartner';
    oData.role = 'CONTRACT_PARTNER';
    return __addBusinessPartner(oData);
}

function __addBusinessPartner(oData) {
  	var namespace = 'org.coma.biznet';
	var factory = getFactory();
  	return getParticipantRegistry(namespace + '.' + oData.ENTITYNAME)
	.then(function(pr){
        var companyId = oData.companyId || oData.name;
        if (!companyId) {
            throw new Error("No candidate for company id found. Please supply either companyId or name");
        }
        var businessPartner = factory.newResource(namespace, oData.ENTITYNAME, companyId || randomString(20));
        oData.name ? businessPartner.name = oData.name : null;
        oData.city ? businessPartner.city = oData.city : null;
        oData.street ? businessPartner.street = oData.street : null;
        oData.role ? businessPartner.roles = [oData.role] : [businessPartner.getClassDeclaration().getDecorators()[0].getArguments()[0]]; // doesn't work yet somehow
        return pr.add(businessPartner);
    });
}

function _addCustomerProjectLead(oData) {
    oData.ENTITYNAME = "CustomerProjectLead";
    oData.role = "PROJECT_LEAD";
    oData.COMPANYTYPE = "Customer";
    return __addPerson(oData);
}

function _addCustomerDeliveryUnit(oData) {
    oData.ENTITYNAME = "CustomerDeliveryUnit";
    oData.role = "DELIVERY_UNIT";
    oData.COMPANYTYPE = "Customer";
    return __addPerson(oData);
}

function _addCustomerDeliveryUnit(oData) {
    oData.ENTITYNAME = "CustomerDeliveryUnit";
    oData.role = "DELIVERY_UNIT";
    oData.COMPANYTYPE = "Customer";
    return __addPerson(oData);
}

function _addCustomerBackOffice(oData) {
    oData.ENTITYNAME = "CustomerBackOffice";
    oData.role = "BACK_OFFICE";
    oData.COMPANYTYPE = "Customer";
    return __addPerson(oData);
}

function _addCustomerPurchasing(oData) {
    oData.ENTITYNAME = "CustomerPurchasing";
    oData.role = "PURCHASING";
    oData.COMPANYTYPE = "Customer";
    return __addPerson(oData);
}

function _addCustomerObserver(oData) {
    oData.ENTITYNAME = "CustomerObserver";
    oData.role = "OBSERVER";
    oData.COMPANYTYPE = "Customer";
    return __addPerson(oData);
}

function _addDeliveryProjectLead(oData)  {
    oData.ENTITYNAME = "DeliveryProjectLead";
    oData.role = "PROJECT_LEAD";
    oData.COMPANYTYPE = "DeliveryPartner";
    return __addPerson(oData);
}

function _addDeliveryBackOffice(oData)  {
    oData.ENTITYNAME = "DeliveryBackOffice";
    oData.role = "BACK_OFFICE";
    oData.COMPANYTYPE = "DeliveryPartner";
    return __addPerson(oData);
}

function _addDeliveryObserver(oData)  {
    oData.ENTITYNAME = "DeliveryObserver";
    oData.role = "OBSERVER";
    oData.COMPANYTYPE = "DeliveryPartner";
    return __addPerson(oData);
}

function _addDeliveryConsultant(oData)  {
    oData.ENTITYNAME = "DeliveryConsultant";
    oData.role = "CONSULTANT";
    oData.COMPANYTYPE = "DeliveryPartner";
    return __addPerson(oData);
}

function _addContractingResponsible(oData) {
    oData.ENTITYNAME = "ContractingResponsible";
    oData.role = "OBSERVER";
    oData.COMPANYTYPE = "DeliveryPartner";
    return __addPerson(oData);
}

function _addOrder (oOrder) {
    var assetRegistry;
    var entity = "org.coma.biznet.Order"
    var factory = getFactory();
    var namespace = "org.coma.biznet";
    return getAssetRegistry(entity)
    .then(function(ar){
        assetRegistry = ar;
        var order = factory.newResource("org.coma.biznet", "Order", oOrder.orderId);
        console.log(order);
        for (var prop in oOrder) {
            order[prop] = oOrder[prop];
        }
        oOrder.customer ? order.customer = factory.newRelationship(namespace, "Customer", oOrder.customer) : null;
        oOrder.customerProjectLead ? order.customerProjectLead = factory.newRelationship(namespace, "CustomerProjectLead", oOrder.customerProjectLead) : null;
        oOrder.customerBackOffice ?  order.customerBackOffice = factory.newRelationship(namespace, "CustomerBackOffice", oOrder.customerBackOffice) : null;
        oOrder.customerPurchasing ? order.customerPurchasing = factory.newRelationship(namespace, "CustomerPurchasing", oOrder.customerPurchasing) : null;
        oOrder.contractPartner ? order.contractPartner = factory.newRelationship(namespace, "ContractPartner", oOrder.contractPartner) : null;
        oOrder.deliveryPartner ? order.deliveryPartner = factory.newRelationship(namespace, "DeliveryPartner", order.deliveryPartner) : null;
        oOrder.deliveryProjectLead ? order.deliveryProjectLead = factory.newRelationship(namespace, "DeliveryProjectLead", oOrder.deliveryProjectLead) : null;
        oOrder.deliveryBackOffice ? order.deliveryBackOffice = factory.newRelationship(namespace, "DeliveryBackOffice", oOrder.deliveryBackOffice) : null;
        oOrder.startDate ? order.startDate = new Date(oOrder.startDate) : null;
        oOrder.endDate ? order.endDate = new Date(oOrder.endDate) : null;
        order.creationDate = new Date();
        order.lastUpdateDate = new Date();
        if (oOrder.deliveryConsultants && oOrder.deliveryConsultants.length) {
            order.deliveryConsultants = [];
            for (var i = 0; i < oOrder.deliveryConsultants.length; i++) {
                order.deliveryConsultants.push(factory.newRelationship(namespace, "DeliveryConsultant", oOrder.deliveryConsultants[i]));
            }
        }
        if (oOrder.customerObservers && oOrder.customerObservers.length) {
            order.customerObservers = [];
            for (var i = 0; i < oOrder.customerObservers.length; i++) {
                order.customerObservers.push(factory.newRelationship(namespace, "CustomerObservers", oOrder.customerObservers[i]));
            }
        }
        if (oOrder.deliveryObservers && oOrder.deliveryObservers.length) {
            order.deliveryObservers = [];
            for (var i = 0; i < oOrder.deliveryObservers.length; i++) {
                order.deliveryObservers.push(factory.newRelationship(namespace, "DeliveryObservers", oOrder.deliveryObservers[i]));
            }
        }
        console.log(order);
        return ar.add(order)
    });
}

function __addPerson(oData) {
    var namespace = 'org.coma.biznet';
    var factory = getFactory();
    if (!oData.ENTITYNAME) {
        throw new Error("Entityname for Person creation not supplied");
    }
    return getParticipantRegistry(namespace + '.' + oData.ENTITYNAME)
    .then(function(pr){
        var userId = oData.userId || randomString(20);
        var person = factory.newResource(namespace, oData.ENTITYNAME, userId);
        var companyRelationship = factory.newRelationship(namespace, oData.COMPANYTYPE, oData.companyId);
        oData.firstName ? person.firstName = oData.firstName : null;
        oData.lastName ? person.lastName = oData.lastName : null;
        oData.email ? person.email = oData.email : null;
        oData.role ? person.roles = [oData.role] : person.getClassDeclaration().getDecorator("defaultRole").getArguments()[0];
        oData.companyId ? person.company = companyRelationship : null;
        return pr.add(person);
    })
}


/**
 * Setup Demo
 * @param {org.coma.biznet.SetupDemo} SetupDemo
 * @transaction
 */
function onSetupDemo(SetupDemo) {
    // set up Business Partners
    onAdmiTransaction();
    return onDeleteAll({really: true})
    .then(function() {
        return _addCustomer({name: 'merck', city: 'Darmstadt', street: 'Frankfurter Straße 250'})
    })
    .then(function() {
        return _addCustomer({name: 'Roche', city: 'Basel', street: 'Mario Basler Straße 123'})
    })
    .then(function() {
        return _addDeliveryPartner({name: 'cbs', city: 'Heidelberg', street: 'Im Breitspiel 19', preferredDeliveryPartner: true})
    })
  	.then(function() {
      	return _addDeliveryPartner({name: 'Accenture', city: 'Heidelberg', street: 'Im Breitspiel 5', preferredDeliveryPartner: false})
    })
  	.then(function() {
        return _addContractPartner({name: 'GULP'})
    })
    .then(function() {
        return _addContractingResponsible({firstName: 'Max', lastName: 'Mustermann', email: 'max.mustermann@gulp.de', companyId: 'GULP', userId: "max.mustermann"})
    })
    .then(function() {
        return _addCustomerBackOffice({firstName: 'Dagmar', lastName: 'Tischer', email: 'dagmar.tischer@merckgroup.com', companyId: 'merck', userId: "B19961"})
    })
    .then(function(){
        return _addCustomerDeliveryUnit({firstName: 'Bjoern', lastName: 'Ebeling', email: 'bjoern.ebeling@merckgroup.com', companyId: 'merck', userId: "M165566"})
    })
    .then(function () {
        return _addCustomerObserver({firstName: 'Nico', lastName: 'Baldanzi', email: 'nico.baldanzi@merckgroup.com', companyId: 'merck', userId: "M140624"})
    })
    .then(function () {
        return _addCustomerProjectLead({firstName: 'Patric', lastName: 'Rueckelt', email: 'patric.rueckelt@merckgroup.com', companyId: 'merck', userId: "M165467"})
    })
    .then(function () {
        return _addCustomerPurchasing({firstName: 'Christine', lastName: 'Glodny', email: 'christine.glodny@merckgroup.com', companyId: 'merck', userId: "M153708"})
    })
    .then(function(){
        return _addDeliveryBackOffice({firstName: 'Bianca', lastName: 'Pointer', email: 'bianca.pointer@cbs-consulting.de', companyId : 'cbs', userId: "bianca.pointer"})
    })
    .then(function() {
        return _addDeliveryConsultant({firstName: 'Jakob', lastName: 'Ruf', email: 'jakob.ruf@cbs-consulting.de', companyId: 'cbs', userId: "X179962"})
    })
    .then(function() {
        return _addDeliveryConsultant({firstName: 'Christoph', lastName: 'Ueberle', email: 'christoph.ueberle@cbs-consulting.de', companyId: 'cbs', userId: "X186835"})
    })
    .then(function() {
        return _addDeliveryConsultant({firstName: 'Talha', lastName: 'Guenduez', email: 'talha.guenduez@cbs-consulting.de', companyId: 'cbs', userId: "X196664"})
    })
    .then(function() {
        return _addDeliveryObserver({firstName: 'Harald', lastName: 'Sulovsky', email: 'harald.sulovsky@merckgroup.com', companyId: 'cbs', userId: "harald.sulovsky"})
    })
    .then(function() {
        return _addDeliveryProjectLead({firstName: 'Jakob', lastName: 'Ruf', email: 'jakob.ruf@cbs-consulting.de', companyId: 'cbs', userId: "X179962"})
    })
    .then(function() {
        return _addDeliveryProjectLead({firstName: 'Denny', lastName: 'Schreber', email: 'denny.schreber@cbs-consulting.de', companyId: 'cbs', userId: "X179960"})
    })
    .then(function() {
        return _addOrder({
            "$class": "org.coma.biznet.Order",
            "orderId": "JORE2",
            "orderName": "JobRequisition 2.0",
            "scope": "Implement additional fields for existing job requisition app",
            "skillKnowledge": "1 Senior Consultant",
            "orderType": "TIME_AND_MATERIAL",
            "orderStatus": "DRAFT",
            "purchaseOrder": "PO38823-A",
            "AccountAssignmet": "AssignmentA",
            "customer": "merck",
            "customerProjectLead": "M125935",
            "customerBackOffice": "B19961",
            "customerPurchasing": "M153708",
            "contractPartner": "GULP",
            "deliveryPartner": "cbs",
            "deliveryProjectLead": "X179960",
            "deliveryConsultants": [
              "X179962"
            ],
            "deliveryBackOffice": "bianca.pointer",
            "initialEstimate": "20 man days",
            "startDate": "2018-04-01T18:48:29.533Z",
            "endDate": "2018-07-01T18:48:29.533Z",
            "dpAssumptions": "Support on SFSF side by Daniel/Dalton\nIntegration support by Sarah",
            "dpComments": "Jakob übernimmt, hat JORE1 gemacht",
            "cusComments": "We need to take other HR Suite Go Lives into account as well"
          })
    })
    .catch(function(error){
        console.log(error);
        console.log(error.stack);
        throw error;
    });
}


function _recursiveDeleteAllParticipants(index){
    index = index || 0;
    var baseNamespace = 'org.coma.biznet.';
    var registry;
    var aEntities = [
        "ContractingResponsible",
        "ContractPartner",
        "Customer",
        "CustomerBackOffice",
        "CustomerDeliveryUnit",
        "CustomerObserver",
        "CustomerProjectLead",
        "CustomerPurchasing",
        "DeliveryBackOffice",
        "DeliveryConsultant",
        "DeliveryObserver",
        "DeliveryPartner",
        "DeliveryProjectLead"
    ];
    var path = baseNamespace + aEntities[index];
    return getParticipantRegistry(path)
    .then(function(pr){
        registry = pr;
        return registry.getAll(path);
    })
    .then(function(entities){
        var aIds = entities.map(function(x){ return x.getIdentifier()});
        return registry.removeAll(aIds);
    })
    .then(function(){
        index += 1;
        if (aEntities[index]) {
            return _recursiveDeleteAllParticipants(index);
        } else {
            return Promise.resolve(true);
        }
    })
    .catch(function(error){
        console.log(error);
        console.log(error.stack);
        throw error;
    });
}

function _recursiveDeleteAllAssets(index){
    var baseNamespace = 'org.coma.biznet.';
    var registry;
    index = index || 0;
    var aEntities = [
        "Order",
    ];
    var path = baseNamespace + aEntities[index]
    return getAssetRegistry(path)
    .then(function(ar){
        registry = ar;
        return registry.getAll(path);
    })
    .then(function(entities){
        var aIds = entities.map(function(x){ return x.getIdentifier()});
        return registry.removeAll(aIds);
    })
    .then(function(){
        index += 1;
        if (aEntities[index]) {
            return _recursiveDeleteAllAssets(index);
        } else {
            return Promise.resolve(true);
        }
    })
    .catch(function(error){
        console.log(error);
        console.log(error.stack);
        throw error;
    });
}


/**
 * Delete All
 * @param {org.coma.biznet.DeleteAll} DeleteAll
 * @transaction
 */
function onDeleteAll(DeleteAll) {
    onAdmiTransaction();
    if (!DeleteAll.really) {
        return
    }
    return _recursiveDeleteAllParticipants()
    .then(function(){
        return _recursiveDeleteAllAssets();
    });
}

/**
 * Upload PDF for hasing
 * @param {org.coma.biznet.UploadPDF} UploadPDF
 * @transaction
 */
function onUploadPDF(UploadPDF) {
    var sPDFHash = hashPDF(UploadPDF.pdfString);
    var assetRegistry;
    var factory = getFactory();
    var relatedOrder;
    var namespace = 'org.coma.biznet';
  	console.log("set up variables");
  	console.log("pdfhash: " + sPDFHash);
    return getAssetRegistry('org.coma.biznet.Order')
    .then(function(ar) {
      	console.log("Got assetRegistry");
      	console.log(ar);
        console.log("related order;")
        console.log(UploadPDF.relatedOrder);
        assetRegistry = ar;
        return ar.get(UploadPDF.relatedOrder.orderId);
    })
    .then(function(order){
      	console.log("got order");
      	console.log(order);
        relatedOrder = order;
        order.pdfHash = sPDFHash;
        order.lastUpdateDate = new Date();
        return assetRegistry.update(order)
    })
    .then(function(){
      	console.log("updated order");
        var PDFUploaded = factory.newEvent('org.coma.biznet', 'PDFUploaded')
        console.log("got event")
        PDFUploaded.relatedOrder = relatedOrder;
        return emit(PDFUploaded);
    });
}


/**
 * Fill Order with information
 * @param {org.coma.biznet.FillOrder} FillOrder
 * @transaction
 */
function onFillOrder(FillOrder) {
    var assetRegistry;
    var factory = getFactory();

    var order = FillOrder.relatedOrder;

    return getAssetRegistry('org.coma.biznet.Order')
    .then(function (orderRegistry) {
        // add the updated info to the shipment
        order.skillKnowledge = FillOrder.skillKnowledge;
        order.orderType = FillOrder.orderType;
        order.startDate = FillOrder.startDate;
        order.endDate = FillOrder.endDate;
        order.dpAssumptions = FillOrder.dpAssumptions;
        order.dpComments = FillOrder.dpComments;
        order.lineItems = FillOrder.lineItems;
        order.lastUpdateDate = new Date();
        return orderRegistry.update(order);
    })
    .then(function() {
        var OrderFilled = factory.newEvent('org.coma.biznet', 'OrderFilled');
        OrderFilled.relatedOrder = order;
        return emit(OrderFilled);
    })
}

/**
 * Set order status RFI_CLARIFICATION by DP
 * @param {org.coma.biznet.SetStatusRFI_CLARIFICATION} SetStatusRFI_CLARIFICATION
 * @transaction
 */
function onSetStatusRFI_CLARIFICATION(SetStatusRFI_CLARIFICATION) {
    onDpTransaction();
    var assetRegistry;
    var factory = getFactory();
    var id = SetStatusRFI_CLARIFICATION.relatedOrder.orderId;
    var relatedOrder;
    return getAssetRegistry('org.coma.biznet.Order')
    .then(function(ar) {
        assetRegistry = ar;
        return assetRegistry.get(id);
    })
    .then(function(order){
        relatedOrder = order;
        if (order.orderStatus !== 'RFI_DP') {
            throw new Error('Status is not RFI_DP');
        }
        order.orderStatus = 'RFI_CLARIFICATION';
        order.lastUpdateDate = new Date();
        return assetRegistry.update(order);
    })
    .then(function(){
        var eventRFI_CLARIFICATION = factory.newEvent('org.coma.biznet', 'SetRFI_CLARIFICATION');
        eventRFI_CLARIFICATION.relatedOrder = relatedOrder;
        return emit(eventRFI_CLARIFICATION);
    });
}

/**
 * Set order status RFQ_CP by DP
 * @param {org.coma.biznet.SetStatusRFQ_CP} SetStatusRFQ_CP
 * @transaction
 */
function onSetStatusRFQ_CP(SetStatusRFQ_CP) {
    onDpTransaction();
    var assetRegistry;
    var factory = getFactory();
    var id = SetStatusRFQ_CP.relatedOrder.orderId;
    var relatedOrder;
    return getAssetRegistry('org.coma.biznet.Order')
    .then(function(ar) {
        assetRegistry = ar;
        return assetRegistry.get(id);
    })
    .then(function(order){
        relatedOrder = order;
        if (order.orderStatus !== 'RFQ_DP') {
            throw new Error('Status is not RFQ_DP');
        }
        order.orderStatus = 'RFQ_CP';
        order.lastUpdateDate = new Date();
        return assetRegistry.update(order);
    })
    .then(function(){
        var eventRFQ_CP = factory.newEvent('org.coma.biznet', 'SetRFQ_CP');
        eventRFQ_CP.relatedOrder = relatedOrder;
        return emit(eventRFQ_CP);
    });
}

/**
 * Set order status by Admin
 * @param {org.coma.biznet.SetStatusAdmin} SetStatusAdmin
 * @transaction
 */
function onSetStatusAdmin(SetStatusAdmin) {
    onAdmiTransaction();
    var assetRegistry;
    var factory = getFactory();
    var id = SetStatusAdmin.relatedOrder.orderId;
    var relatedOrder;
    return getAssetRegistry('org.coma.biznet.Order')
    .then(function(ar) {
        assetRegistry = ar;
        return assetRegistry.get(id);
    })
    .then(function(order){
        order.orderStatus = SetStatusAdmin.newStatus
        order.lastUpdateDate = new Date();
        return assetRegistry.update(order);
    })
}

/**
 * Set order status to RFI_DP by Customer
 * @param {org.coma.biznet.SetStatusRFI_DP} SetStatusRFI_DP
 * @transaction
 */
function onSetStatusRFI_DP(SetStatusRFI_DP) {
    onCusTransaction();
    var assetRegistry;
    var factory = getFactory();
    var id = SetStatusRFI_DP.relatedOrder.orderId;
    var relatedOrder;
    return getAssetRegistry('org.coma.biznet.Order')
    .then(function(ar) {
        assetRegistry = ar;
        return assetRegistry.get(id);
    })
    .then(function(order){
        relatedOrder = order;
        if (order.orderStatus !== 'DRAFT') {
            throw new Error('Status is not DRAFT');
        }
        order.orderStatus = 'RFI_DP';
        order.lastUpdateDate = new Date();
        return assetRegistry.update(order);
    })
    .then(function(){
        var eventRFI_DP = factory.newEvent('org.coma.biznet', 'SetRFI_DP');
        eventRFI_DP.relatedOrder = relatedOrder;
        return emit(eventRFI_DP);
    });
}

/**
 * Set order status to RFI_CONFIRM by Customer
 * @param {org.coma.biznet.SetStatusRFI_CONFIRM} SetStatusRFI_CONFIRM
 * @transaction
 */
function onSetStatusRFI_CONFIRM(SetStatusRFI_CONFIRM) {
    onCusTransaction();
    var assetRegistry;
    var factory = getFactory();
    var id = SetStatusRFI_CONFIRM.relatedOrder.orderId;
    var relatedOrder;
    return getAssetRegistry('org.coma.biznet.Order')
    .then(function(ar) {
        assetRegistry = ar;
        return assetRegistry.get(id);
    })
    .then(function(order){
        relatedOrder = order;
        if (order.orderStatus !== 'RFI_CLARIFICATION') {
            throw new Error('Status is not RFI_CLARIFICATION');
        }
        order.orderStatus = 'RFI_CONFIRM';
        order.lastUpdateDate = new Date();
        return assetRegistry.update(order);
    })
    .then(function(){
        var eventRFI_CONFIRM = factory.newEvent('org.coma.biznet', 'SetRFI_CONFIRM');
        eventRFI_CONFIRM.relatedOrder = relatedOrder;
        return emit(eventRFI_CONFIRM);
    });
}

/**
 * Set order status to DRAFT by Customer
 * @param {org.coma.biznet.SetStatusDRAFT} SetStatusDRAFT
 * @transaction
 */
function onSetStatusDRAFT(SetStatusDRAFT) {
    onCusTransaction();
    var assetRegistry;
    var factory = getFactory();
    var id = SetStatusDRAFT.relatedOrder.orderId;
    var relatedOrder;
    return getAssetRegistry('org.coma.biznet.Order')
    .then(function(ar) {
        assetRegistry = ar;
        return assetRegistry.get(id);
    })
    .then(function(order){
        relatedOrder = order;
        if (order.orderStatus !== 'RFI_CLARIFICATION') {
            throw new Error('Status is not RFI_CLARIFICATION');
        }
        order.orderStatus = 'DRAFT';
        order.lastUpdateDate = new Date();
        return assetRegistry.update(order);
    })
    .then(function(){
        var eventDRAFT = factory.newEvent('org.coma.biznet', 'SetDRAFT');
        eventDRAFT.relatedOrder = relatedOrder;
        return emit(eventDRAFT);
    });
}

/**
 * Set order status to RFQ_DP by Customer
 * @param {org.coma.biznet.SetStatusRFQ_DP} SetStatusRFQ_DP
 * @transaction
 */
function onSetStatusRFQ_DP(SetStatusRFQ_DP) {
    onCusTransaction();
    var assetRegistry;
    var factory = getFactory();
    var id = SetStatusRFQ_DP.relatedOrder.orderId;
    var relatedOrder;
    return getAssetRegistry('org.coma.biznet.Order')
    .then(function(ar) {
        assetRegistry = ar;
        return assetRegistry.get(id);
    })
    .then(function(order){
        relatedOrder = order;
        if (order.orderStatus !== 'RFI_CONFIRM') {
            throw new Error('Status is not RFI_CONFIRM');
        }
        order.orderStatus = 'RFQ_DP';
        order.lastUpdateDate = new Date();
        return assetRegistry.update(order);
    })
    .then(function(){
        var eventRFQ_DP = factory.newEvent('org.coma.biznet', 'SetRFQ_DP');
        eventRFQ_DP.relatedOrder = relatedOrder;
        return emit(eventRFQ_DP);
    });
}

/**
 * Set order status to RFQ_DP by Customer
 * @param {org.coma.biznet.SetStatusQUOTATION_REVIEW} SetStatusQUOTATION_REVIEW
 * @transaction
 */
function onSetStatusQUOTATION_REVIEW(SetStatusQUOTATION_REVIEW) {
    var sPDFHash = hashPDF(SetStatusQUOTATION_REVIEW.documentBase64String);
    var assetRegistry;
    var factory = getFactory();
    var id = SetStatusQUOTATION_REVIEW.relatedOrder.orderId;
    var relatedOrder = SetStatusQUOTATION_REVIEW.relatedOrder;

    var document = factory.newResource('org.coma.biznet', 'Document', sPDFHash);
    document.fileName = SetStatusQUOTATION_REVIEW.documentFileName;
    document.documentType = "CP_OFFER";
    document.filePath = SetStatusQUOTATION_REVIEW.documentFilePath;

    if(relatedOrder.documents){
        relatedOrder.documents.push(document);
    }else{
        relatedOrder.documents = [document];
    }
    if (relatedOrder.orderStatus !== 'RFQ_CP') {
        throw new Error('Status is not RFQ_CP');
    }
    relatedOrder.orderStatus = 'QUOTATION_REVIEW';
    relatedOrder.lastUpdateDate = new Date();

    return getAssetRegistry('org.coma.biznet.Document')
        .then(function (docRegistry) {
            // add the boxes
            return docRegistry.add(document);
        })
        .then(function() {
            return getAssetRegistry('org.coma.biznet.Order')
        })
        .then(function(orderRegistry) {
            // update the order
            return orderRegistry.update(relatedOrder);
        })
        .then(function(){
            var eventRFQ_DP = factory.newEvent('org.coma.biznet', 'SetRFQ_DP');
            eventRFQ_DP.relatedOrder = relatedOrder;
            return emit(eventRFQ_DP);
        });
        
}

/**
 * Set order status to REQ_PR by Customer
 * @param {org.coma.biznet.SetStatusREQ_PR} SetStatusREQ_PR
 * @transaction
 */
function onSetStatusREQ_PR(SetStatusREQ_PR) {
    onCusTransaction();
    var assetRegistry;
    var factory = getFactory();
    var id = SetStatusREQ_PR.relatedOrder.orderId;
    var relatedOrder;
    return getAssetRegistry('org.coma.biznet.Order')
    .then(function(ar) {
        assetRegistry = ar;
        return assetRegistry.get(id);
    })
    .then(function(order){
        relatedOrder = order;
        if (order.orderStatus !== 'QUOTATION_REVIEW') {
            throw new Error('Status is not QUOTATION_REVIEW');
        }
        order.orderStatus = 'REQ_PR';
        order.lastUpdateDate = new Date();
        return assetRegistry.update(order);
    })
    .then(function(){
        var eventREQ_PR = factory.newEvent('org.coma.biznet', 'SetREQ_PR');
        eventREQ_PR.relatedOrder = relatedOrder;
        return emit(eventREQ_PR);
    });
}

/**
 * Set order status to REQ_PO by Customer
 * @param {org.coma.biznet.SetStatusREQ_PO} SetStatusREQ_PO
 * @transaction
 */
function onSetStatusREQ_PO(SetStatusREQ_PO) {
    onCusTransaction();
    var assetRegistry;
    var factory = getFactory();
    var id = SetStatusREQ_PO.relatedOrder.orderId;
    var relatedOrder;
    return getAssetRegistry('org.coma.biznet.Order')
    .then(function(ar) {
        assetRegistry = ar;
        return assetRegistry.get(id);
    })
    .then(function(order){
        relatedOrder = order;
        if (order.orderStatus !== 'REQ_PR') {
            throw new Error('Status is not REQ_PR');
        }
        order.orderStatus = 'REQ_PO';
        order.lastUpdateDate = new Date();
        return assetRegistry.update(order);
    })
    .then(function(){
        var eventREQ_PO = factory.newEvent('org.coma.biznet', 'SetREQ_PO');
        eventREQ_PO.relatedOrder = relatedOrder;
        return emit(eventREQ_PO);
    });
}

/**
 * Set order status to REQ_PROJECT_AGREEMENT by Customer
 * @param {org.coma.biznet.SetStatusREQ_PROJECT_AGREEMENT} SetStatusREQ_PROJECT_AGREEMENT
 * @transaction
 */
function onSetStatusREQ_PROJECT_AGREEMENT(SetStatusREQ_PROJECT_AGREEMENT) {
    onCusTransaction();
    var assetRegistry;
    var factory = getFactory();
    var id = SetStatusREQ_PROJECT_AGREEMENT.relatedOrder.orderId;
    var relatedOrder;
    return getAssetRegistry('org.coma.biznet.Order')
    .then(function(ar) {
        assetRegistry = ar;
        return assetRegistry.get(id);
    })
    .then(function(order){
        relatedOrder = order;
        if (order.orderStatus !== 'REQ_PO') {
            throw new Error('Status is not REQ_PO');
        }
        order.orderStatus = 'REQ_PROJECT_AGREEMENT';
        order.purchaseOrder = SetStatusREQ_PROJECT_AGREEMENT.purchaseOrder;
        order.AccountAssignmet = SetStatusREQ_PROJECT_AGREEMENT.AccountAssignmet;
        order.lastUpdateDate = new Date();
        return assetRegistry.update(order);
    })
    .then(function(){
        var eventREQ_PROJECT_AGREEMENT = factory.newEvent('org.coma.biznet', 'SetREQ_PROJECT_AGREEMENT');
        eventREQ_PROJECT_AGREEMENT.relatedOrder = relatedOrder;
        return emit(eventREQ_PROJECT_AGREEMENT);
    });
}

/**
 * Set order status to ORDER_CANCELLED by Customer
 * @param {org.coma.biznet.SetStatusORDER_CANCELLED} SetStatusORDER_CANCELLED
 * @transaction
 */
function onSetStatusORDER_CANCELLED(SetStatusORDER_CANCELLED) {
    onCusTransaction();
    var assetRegistry;
    var factory = getFactory();
    var id = SetStatusORDER_CANCELLED.relatedOrder.orderId;
    var relatedOrder;
    return getAssetRegistry('org.coma.biznet.Order')
    .then(function(ar) {
        assetRegistry = ar;
        return assetRegistry.get(id);
    })
    .then(function(order){
        relatedOrder = order;
        order.orderStatus = 'SetStatusORDER_CANCELLED';
        order.lastUpdateDate = new Date();
        return assetRegistry.update(order);
    })
    .then(function(){
        var eventORDER_CANCELLED = factory.newEvent('org.coma.biznet', 'SetORDER_CANCELLED');
        eventORDER_CANCELLED.relatedOrder = relatedOrder;
        return emit(eventORDER_CANCELLED);
    });
}