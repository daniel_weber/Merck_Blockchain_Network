# org.coma.biznet

> This network tracks the Lifecycle of an consulting order from the quotation process until the order placement. 

This business network defines:

**Participants**
`Customer` `CustomerDeliveryUnit` `CustomerProjectLead` `CustomerBackOffice` `CustomerPurchasing` `CustomerObserver` `DeliveryPartner` `DeliveryProjectLead` `DeliveryConsultant` `DeliveryBackOffice` `DeliveryObserver` `ContractPartner` `ContractingResponsible`
Customer
**Assets**
`Order`

**Transactions**
`SetupDemo` `DeleteAll` `SubmitEstimate` `CloseOrder` `FillOrder` `FirstEstimation` �AcceptEstimation� �DeclineEstimation� �ProjectCC�
�submitformaloffer� �purchaserequest�



**Events**
`EstimateSubmittedEvent`


The ` CustomerProjectLead ` is creating a new order and asking the `DeliveryPartner` to submit a `SubmitEstimate` transaction. The ` CustomerProjectLead ` can always close or cancel the order with submitting a `CloseOrder` transaction. Status DRAFT switch to status RFI. 


`DeliveryPartner` submit the requested `FirstEstimation` (TimeLine, Scope) to ` CustomerDeliveryUnit`. Status RFI switch to status RFI Clarification. 

???

`CustomerProjectLead` must decide if adjustment of `FirstEstimation` is needed. `CustomerProjectLead` can �AcceptEstimation� or �DeclineEstimation�. 

In case the `FirstEstimation` has been declined. ` CustomerProjectLead ` is required to create a new order. 

In case the `CustomerProjectLead` accepted the `FirstEstimation` `CustomerProjectLead` is asking ` CustomerDeliveryUnit` to request a quote from `DeliveryPartner` and `ContractPartner`. Status RFI Clarification switch to status RFI Confirmation.


`CustomerDeliveryUnit ` request from `DeliveryPartner` a formal offer. Status RFI Confirmation switch to status RFQ DP.



�DeliveryPartner` �submitformaloffer� to `ContractPartner`. Status RFI Confirmation switch to status RFQ DP.

`CustomerProjectLead` submit �purchaserequest� to `ContractPartner�. 




To test this Business Network Definition in the **Test** tab:

Submit a `SetupDemo` transaction. 

This transaction populates the Participant Registries. 
